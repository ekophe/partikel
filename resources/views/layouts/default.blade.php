<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Sterile Website</title>
<link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('/css/mystyle.css') }}">
</head>
<header>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      {!!Html::link('/', 'Home', array('class' => 'navbar-brand'))!!}
      
    </div>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      @if (Auth::user())
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Visine <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li>{!!Html::link('/contvisine', 'Continuous Particle Monitoring')!!}</li>
            <!--
            <li><a href="#">Descrete Partikel Monitoting</a></li>
            <li class="divider"></li>
            <li><a href="#">Event sediaan Visine</a></li> 
            //fitur belum akan dikerjakan
            -->
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Injeksi <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li>{!!Html::link('/continjeksi', 'Continuous Particle Monitoring')!!}</li>
            <!--
            <li><a href="#">Descrete Partikel Monitoting</a></li>
            <li class="divider"></li>
            <li><a href="#">Event sediaan Injeksi</a></li>
            //fitur belum akan dikerjakan
            -->
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Report <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li>{!!Html::link('/contvisine/reports', 'Visine')!!}</li>
            <li>{!!Html::link('/continjeksi/reports', 'Injeksi')!!}</li>
          </ul>
        </li>

        @endif
        <ul class="nav navbar-nav navbar-right">
        					@if (Auth::guest())
        						<li><a href="{{ url('/auth/login') }}">Login</a></li>
        						<li><a href="{{ url('/auth/register') }}">Register</a></li>
        					@else
        						<li class="dropdown">
        							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
        							<ul class="dropdown-menu" role="menu">
        								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
        							</ul>
        						</li>
        					@endif
        				</ul>
 </div>           
</div>
</header>

<body>

	<section class="container-fluid">
    <div class="content row">
    	<section class="main col col-md-10">
        @yield('content')
        </section>
        <section class="sidebar col col-md-2">
        @yield('sidebar')
        </section>
    </div>
    </section>

<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>

@yield('footer')
</body>
</html>