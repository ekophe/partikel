@extends('layouts.default')

@section('content')

<div id="contvisine-index">
	<h1>Continuous Particle Monitoring Visine</h1>

	<p>
	    {!!Html::link('/contvisine/create', 'Create a particle monitoring', array('class'=>'btn btn-primary btn-lg'))!!}
	    {!!Html::link('/contvisine/export', 'Export Data Partikel Monitoring', array('class'=>'btn btn-primary btn-lg'))!!}
	</p>
	<table border="1" class="table table-striped">
		<tr>
			<th>Lot No</th>
			<th>Tahun</th>
			<th>alarm Occured (%)</th>
            <th>Action</th>
		</tr>
		
		@foreach($contvisines as $contvisine)
			<tr>
				<td>{!!$contvisine->lot_no!!}</td>
				<td>{!!$contvisine->tahun!!}</td>
				<td>{!!$contvisine->alarm_occur!!}</td>
                <td>
                    {!! Form::open(array('url' => 'contvisine/' . $contvisine->id, 'method' => 'delete')) !!}
                    <a href="/contvisine/{{$contvisine->id}}/edit">{!!Html::image('img/edit.gif', 'Remove product')!!}</a>
                    <a href="/contvisine/{{$contvisine->id}}">{!!Html::image('img/details.gif', 'Remove product')!!}</a>
                    <input type="image" src="img/delete.gif" alt="Submit Form" />
                    {!! Form::close() !!}
				</td>

			</tr>
		@endforeach
	</table>
</div>
@stop