@extends('layouts.default')

@section('content')
<h1>Visine Continuous Particle Monitoring</h1>

<canvas id="visine-cont" width="600" height="300"></canvas>

@stop


@section('footer')
    <script src="{{ asset('js/chart.min.js') }}"></script>

    <script>
        (function(){
            var ctx = document.getElementById('visine-cont').getContext('2d');
            var chart = {
                labels: {!! json_encode($lotNo)!!},
                datasets: [{
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: {!! json_encode($alarmOccur)!!}
                }]
            };

            new Chart(ctx).Line(chart, {bezierCurve: false});
        }) ();
    </script>
@stop