@extends('layouts.default')

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Add Particle Monitoring</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					{!!Form::open(array('url'=>'contvisine', 'class'=>'form-horizontal'))!!}

						<div class="form-group">
							{!!Form::label('lot_no', 'Lot No', array('class'=>'col-md-4 control-label'))!!}
							<div class="col-md-6">
								{!!Form::text('lot_no', null, array('class'=>'form-control'))!!}
							</div>
						</div>

						<div class="form-group">
							{!!Form::label('tahun', 'Tahun', array('class'=>'col-md-4 control-label'))!!}
							<div class="col-md-6">
								{!!Form::text('tahun', null, array('class'=>'form-control'))!!}
							</div>
						</div>

						<div class="form-group">
							{!!Form::label('alarm_occur', 'Alarm Occur (%)', array('class'=>'col-md-4 control-label'))!!}
							<div class="col-md-6">
								{!!Form::text('alarm_occur', null, array('class'=>'form-control'))!!}
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								{!!Form::submit('Create', array('class'=>'btn btn-primary'))!!}
							</div>
						</div>
					{!!Form::close()!!}
				</div>
			</div>
		</div>
	</div>

@endsection
