@extends('layouts.default')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="jumbotron">
                  <h1>Welcome {{ Auth::user()->name }} !!</h1>
                  <p>This is where you can find a process control of sterile production</p>

                </div>
			</div>
		</div>
	</div>
</div>
@endsection
