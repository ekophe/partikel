@extends('layouts.default')

@section('content')

<div id="continjeksi-index">
	<h1>Continuous Particle Monitoring Injeksi</h1>

	<p>
	{!!Html::link('/continjeksi/create', 'Create a particle monitoring', array('class'=>'btn btn-primary btn-lg'))!!}
	{!!Html::link('/continjeksi/export', 'Export Data Partikel Monitoring', array('class'=>'btn btn-primary btn-lg'))!!}
	</p>

	<table border="1" class="table table-striped">
		<tr>
			<th>Lot No</th>
			<th>Tahun</th>
			<th>alarm Occured (%)</th>
			<th>Action</th>

		</tr>

		@foreach($continjeksis as $continjeksi)

			<tr>
				<td>{!!$continjeksi->lot_no!!}</td>
				<td>{!!$continjeksi->tahun!!}</td>
				<td>{!!$continjeksi->alarm_occur!!}</td>
				<td>
                    {!! Form::open(array('url' => 'continjeksi/' . $continjeksi->id, 'method' => 'delete')) !!}
                    <a href="/continjeksi/{{$continjeksi->id}}/edit">{!!Html::image('img/edit.gif', 'Remove product')!!}</a>
                    <a href="/continjeksi/{{$continjeksi->id}}">{!!Html::image('img/details.gif', 'Remove product')!!}</a>
                    <input type="image" src="img/delete.gif" alt="Submit Form" />
                    {!! Form::close() !!}
				</td>

			</tr>

		@endforeach
	</table>
</div>
@stop