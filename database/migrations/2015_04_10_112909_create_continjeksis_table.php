<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContinjeksisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('continjeksis', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('lot_no');
            $table->integer('tahun');
            $table->double('alarm_occur');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('continjeksis');
	}

}
