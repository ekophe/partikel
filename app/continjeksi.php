<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class continjeksi extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'continjeksi';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['lot_no', 'tahun', 'alarm_occur'];



}
