<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
]);

Route::get('contvisine/export', 'contvisineController@export');
Route::get('contvisine/reports', 'contvisineController@reports');
Route::resource('contvisine', 'contvisineController');

Route::get('continjeksi/export', 'continjeksiController@export');
Route::get('continjeksi/reports', 'continjeksiController@reports');
Route::resource('continjeksi', 'continjeksiController');
