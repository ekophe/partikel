<?php namespace App\Http\Controllers;

use App\continjeksi;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class continjeksiController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return view('continjeksi.index')->with('continjeksis', continjeksi::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('continjeksi.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $this->validate($request, [
            'lot_no'=>'required|unique:contvisine|digits:8|integer',
            'tahun'=>'required|digits:4|integer',
            'alarm_occur'=>'required'
        ]);
        continjeksi::create($request->all());

        return Redirect::to('continjeksi');
	}

    public function reports(){

        $reports = continjeksi::all();
        return view('continjeksi.reports')->with('lotNo', $reports->lists('lot_no'))->with('alarmOccur', $reports->lists('alarm_occur'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$continjeksi = continjeksi::findOrFail($id);

        return view('continjeksi.show', compact('continjeksi'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $continjeksi = continjeksi::findOrFail($id);

		return view('continjeksi.edit', compact('continjeksi'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

    public function update($id, Request $request)
	{
		$continjeksi = continjeksi::findOrFail($id);

        $continjeksi->update($request->all());

        return Redirect::to('continjeksi');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $continjeksi = continjeksi::findOrFail($id);

        $continjeksi->delete();

        return Redirect::to('continjeksi');

	}

    public function export(){
        $injeksis = continjeksi::all();

        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());

        $csv->insertOne(\Schema::getColumnListing('continjeksi'));

        foreach ($injeksis as $injeksi) {
            $csv->insertOne($injeksi->toArray());
        }
        $csv->output('injeksi.csv');

    }

}
