<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\contvisine;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class contvisineController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('contvisine.index')->with('contvisines', contvisine::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('contvisine.create');		
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'lot_no'=>'required|unique:contvisine|digits:8|integer',
			'tahun'=>'required|digits:4|integer',
			'alarm_occur'=>'required'
		]);		
		 contvisine::create($request->all());
		 
		 return Redirect::to('contvisine');
		 		 
	}

    public function reports(){

        $reports = contvisine::all();
        return view('contvisine.reports')->with('lotNo', $reports->lists('lot_no'))->with('alarmOccur', $reports->lists('alarm_occur'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $contvisine = contvisine::findOrFail($id);

        return view('contvisine.show', compact('contvisine'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $contvisine = contvisine::findOrFail($id);

        return view('contvisine.edit', compact('contvisine'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
        $contvisine = contvisine::findOrFail($id);

        $contvisine->update($request->all());

        return Redirect::to('contvisine');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $contvisine = contvisine::findOrFail($id);

        $contvisine->delete();

        return Redirect::to('contvisine');
	}

    public function export(){
        $visines = contvisine::all();

        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());

        $csv->insertOne(\Schema::getColumnListing('contvisine'));

        foreach ($visines as $visine) {
            $csv->insertOne($visine->toArray());
        }
        $csv->output('visine.csv');

    }

}
